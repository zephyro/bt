//BookingSlider
function BookingSlider() {
    $('.booking_slider, .booking_slider-right').owlCarousel({
        items: 1,
        lazyLoad: true,
        loop: true,
        singleItem: true,
        autoplay: false,
        autoplayTimeout: 2000,
        autoplaySpeed: 1000,
        smartSpeed: 1500,
        dots: false,
        nav: true,
        navText: ["<div class='a-left'><span></span></div>", "<div class='a-right'><span></span></div>"],
        margin: 20,
        responsiveClass: true,
        responsive: {
            0: {
                items: 0
            },
            480: {
                items: 1,
                autoWidth: false,
                margin: 5
            },
            768: {
                items: 1,
                autoWidth: false,
                margin: 5
            },
            992: {
                items: 1,
                autoWidth: false,
                margin: 10
            },
        }
    })
}

//Responsive Menu
function Responsive_menu() {
    $(".nav_toggle").on('click', function () {
        $(this).toggleClass("toggle_open");
        $(".header_right_menu").toggleClass("menu_open");
    });
}

//animation on page scroll
function wowanimation() {
    var wow = new WOW({
        boxClass: 'wow',      // default
        animateClass: 'animated', // default
        offset: 0,          // default
        mobile: true,       // default
        live: true        // default
    })
    wow.init();
}

/* привязка плагина jquery.formstyler.min.js к селектору ()*/
// if (document.getElementById("select_baggage") != null) {
//     $(function () {
//         $('select').styler();
//     });
// }
if (document.getElementById("boxMatching") != null) {
    $(document).ready(function () {
        $(".btn_matching").click(function () {
            $(".btn_matching").toggleClass("active");
        });
    });
}
/* счетчик */
if (document.getElementById("input-number") != null) {
    (function () {

        window.inputNumber = function (el) {

            var min = el.attr('min') || false;
            var max = el.attr('max') || false;

            var els = {};

            els.dec = el.prev();
            els.inc = el.next();

            el.each(function () {
                init($(this));
            });

            function init(el) {

                els.dec.on('click', decrement);
                els.inc.on('click', increment);

                function decrement() {
                    var value = el[0].value;
                    value--;
                    if (!min || value >= min) {
                        el[0].value = value;
                    }
                }

                function increment() {
                    var value = el[0].value;
                    value++;
                    if (!max || value <= max) {
                        el[0].value = value++;
                    }
                }
            }
        }
    })();

    inputNumber($('.input-number'));
}
// if (document.getElementById("select_tr") != null) {
//     $(function () {
//         $('select').styler();
//     });
// }

/*function moreShowFunction() {
    var dots = document.getElementById("dots");
    var moreText = document.getElementById("more");
    var btnText = document.getElementById("showBtn");

    if (dots.style.display === "none") {
        dots.style.display = "inline";
        btnText.innerHTML = "Полное описание";
        moreText.style.display = "none";
    } else {
        dots.style.display = "none";
        btnText.innerHTML = "Скрыть описание";
        moreText.style.display = "inline";
    }
}*/

if ($(window).width() > 768) {
    if (document.getElementById("showBtnList") != null) { /*раскрыть - закрыть блок со списком на странице с отелем - показать +7*/
        function moreShowListFunction() {
            var dotsList = document.getElementById("dots-list");
            var moreTextList = document.getElementById("more-list");
            var btnTextList = document.getElementById("showBtnList");
            var topCardHeight = document.getElementById("topHeightCard");

            if (dotsList.style.display === "none") {
                dotsList.style.display = "inline";
                btnTextList.innerHTML = "еще 7";
                moreTextList.style.display = "none";
                topCardHeight.style.height = "220px";
            } else {
                topCardHeight.style.height = "auto";
                dotsList.style.display = "none";
                btnTextList.innerHTML = "скрыть";
                moreTextList.style.display = "inline";
            }
        }
    }
} else {
    if (document.getElementById("showBtnList") != null) { /*раскрыть - закрыть блок со списком на странице с отелем - показать +7*/
        function moreShowListFunction() {
            var dotsList = document.getElementById("dots-list");
            var moreTextList = document.getElementById("more-list");
            var btnTextList = document.getElementById("showBtnList");
            var topCardHeight = document.getElementById("topHeightCard");

            if (dotsList.style.display === "none") {
                dotsList.style.display = "inline";
                btnTextList.innerHTML = "еще 7";
                moreTextList.style.display = "none";
                topCardHeight.style.height = "auto";
            } else {
                topCardHeight.style.height = "auto";
                dotsList.style.display = "none";
                btnTextList.innerHTML = "скрыть";
                moreTextList.style.display = "inline";
            }
        }
    }
}

/*раскрыть - закрыть блок со списком сервеса на странице с отелем в выдвигающемся блоке - показать еще*/
if (document.getElementById("showBtnListRight") != null) {
    function moreShowListRightFunction() {
        var dotsListRight = document.getElementById("dots-list-right");
        var moreTextListRight = document.getElementById("more-list-right");
        var btnTextListRight = document.getElementById("showBtnListRight");


        if (dotsListRight.style.display === "none") {
            dotsListRight.style.display = "inline";
            btnTextListRight.innerHTML = "еще 7";
            moreTextListRight.style.display = "none";

        } else {
            dotsListRight.style.display = "none";
            btnTextListRight.innerHTML = "скрыть";
            moreTextListRight.style.display = "inline";
        }
    }

}

//Сохранение информации профиля
$(document).on('click', '.editMyInfoFormButtonSubmit', function(){
    var formData = new FormData($('#editMyInfoForm').get(0));

    if (formData.get('updates_and_news') == null) {
        formData.append('updates_and_news', 0);
    }
    if (formData.get('change_travel_status') == null) {
        formData.append('change_travel_status', 0);
    }
    if (formData.get('new_travel_and_approval') == null) {
        formData.append('new_travel_and_approval', 0);
    }
    if (formData.get('promotions_and_discounts') == null) {
        formData.append('promotions_and_discounts', 0);
    }

    // for(var pair of formData.entries()) {
    //     console.log(pair[0]+ ', '+ pair[1]); 
    // }
    
    $.ajax({
        type: 'POST',
        url: '/index.php?do=savemyinfo',
        data: formData,
        processData: false,
        contentType: false,
        success: function(result){
            result = JSON.parse(result);

            if (result.type == 'success') {
                location.reload();
            } else if (result.type == 'error') {
                var errorHtml = '';

                for (var i = 0; i < result.messages.length; i++) {
                    errorHtml += '<div class="alert alert-danger" role="alert" style="margin: 10px 10px;">'+
                                    result.messages[i]+
                                    '<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
                                        '<span aria-hidden="true">&times;</span>'+
                                    '</button>'+
                                '</div>';
                }

                $('#editProfileErrors').html(errorHtml);
                closeNav0();
                closeNav1();
                closeNav2();
                closeNav3();
                closeNav4();

                $('#myProfile').animate({scrollTop: 0}, 500);
            }
        },
    });
});

$(document).on('click', '#myNotificationsUpdatesAndNews', function(){
    $(this).attr('ckecked', 'checked');
});
$(document).on('click', '#myNotificationsChangeTravelStatus', function(){
    $(this).attr('ckecked', 'checked');
});
$(document).on('click', '#myNotificationsNewTravelAndApproval', function(){
    $(this).attr('ckecked', 'checked');
});
$(document).on('click', '#myNotificationsPromotionsAndDiscounts', function(){
    $(this).attr('ckecked', 'checked');
});

// Привязка плагина jquery.formstyler.min.js
if ($('.myGender').length) {
    $(function () {
        $('.myGender').styler();
    });
}
if ($('.myCitizenship').length) {
    $(function () {
        $('.myCitizenship').styler();
    });
}

/*привязка для страницы отелей*/
// if (document.getElementById("selct_box") != null) {
//     $(function () {
//         $('select').styler();
//     });
// }
/*button */

// toggling classes
/*
$('a.btn-like').on('click', function () {
    $(this).toggleClass('liked');
    $('.like-text,.unlike-text').toggle();
});

$('a.btn-favorite').on('click', function () {
    $(this).toggleClass('liked');
    $('.favorite-text,.unfavorite-text').toggle();
});
*/

/*Для переключения между Билетами и Отелями*/

function openBox(pageName, elmnt, color, color_font) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
        tablinks[i].style.color = "";
    }
    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
    elmnt.style.color = color_font;

    if (pageName == 'Hotels') {
        $('.hotel_search_input').focus();
    }

}

if (document.getElementById("defaultOpen_1") != null) {
    document.getElementById("defaultOpen_1").click(); // для открытия первого таба
}

function miniPopupFunction() {
    var miniPopup = document.getElementById("miniPopup");
    miniPopup.classList.toggle("show");
}


function miniPopupFunction2() {
    var miniPopup = document.getElementById("miniPopup2");
    miniPopup.classList.toggle("show");
}


/*Для переключения между видами транспорта на главной*/

function openPage(pageName, elmnt, color, color_font, border_btn, font_family, class_btn) {
    var i, tabcontent_var, tablinks_var;
    tabcontent_var = document.getElementsByClassName("tabcontent-ticket");
    for (i = 0; i < tabcontent_var.length; i++) {
        tabcontent_var[i].style.display = "none";
    }
    tablinks_var = document.getElementsByClassName("tablink-ticket");
    for (i = 0; i < tablinks_var.length; i++) {
        tablinks_var[i].style.backgroundColor = "";
        tablinks_var[i].style.color = "";
        tablinks_var[i].style.border = "";
        tablinks_var[i].style.fontFamily = "";
    }
    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
    elmnt.style.color = color_font;
    elmnt.style.border = border_btn;
    elmnt.style.fontFamily = font_family;

    $('.' + pageName.toLowerCase() + '_search_input_from').focus();
}

$(document).ready(function () {
    $(".tablink-ticket").each(function () {
        $(this).click(function () {
            $(this).addClass("active_btn");
            $(this).siblings().removeClass("active_btn");
        });
    });
});
if (document.getElementById("defaultOpen_2") != null) {
    document.getElementById("defaultOpen_2").click(); // для открытия второго таба
}

$("button#btnBgTrain").click(function () {
    document.getElementById('bg_btn').style.backgroundImage = "url('/images/bus.png')";
});

$("button#defaultOpen_2").click(function () {
    document.getElementById('bg_btn').style.backgroundImage = "url('/images/avia.png')";
});

$("button#btnBgBus").click(function () {
    document.getElementById('bg_btn').style.backgroundImage = "url('/images/bus.png')";
});
$("button#btnBgHotels").click(function () {
    document.getElementById('bg_btn').style.backgroundImage = "url('/images/hotel.png')";
})
$("button#defaultOpen_1").click(function () {
    document.getElementById('bg_btn').style.backgroundImage = "url('/images/avia.png')";
})

/*для пользоватея выпадающее поле*/

function openUser() {
    document.getElementById("userDropdown").classList.toggle("show");
}

/*для правого окна - Мой профиль*/

function openNav() {
    document.getElementById("myProfile").style.right = "0";
    document.getElementById("overlay").style.display = "block";
    document.body.style.overflow = "hidden";
    document.getElementById("userDropdown").classList.remove("show");

}

function closeNav() {
    document.getElementById("myProfile").style.right = "-680px";
    document.getElementById("overlay").style.display = "none";
    document.body.style.overflow = "auto";
}

function openNavDetailTr() {
    document.getElementById("openRightBlockTr").style.right = "0";
    document.getElementById("overlay").style.display = "block";
    document.body.style.overflow = "hidden";
    document.getElementById("userDropdown").classList.remove("show");
	
	if ($('#openRightBlockTr').length) {
			$(document).mouseup(function (e){ 
				var divs = $("#openRightBlockTr");
				if (!divs.is(e.target)
					&& divs.has(e.target).length === 0
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px" 
					&& $('#myPassword').css('right') == "-680px"
					&& $('#editMyMultiPassport').css('right') == "-680px"
					&& $('#myProfile').css('right') == "-680px"
					&& $('#myNotifications').css('right') == "-680px"){
					document.getElementById("openRightBlockTr").style.right = "-680px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";
				}
			});
			
			$(document).mouseup(function (e){ 
				var divs = $("#myProfile");
				if (!divs.is(e.target)
					&& divs.has(e.target).length === 0
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px" 
					&& $('#myPassword').css('right') == "-680px"
					&& $('#editMyMultiPassport').css('right') == "-680px"
					&& $('#openRightBlockTr').css('right') == "-680px"
					&& $('#myNotifications').css('right') == "-680px"){
					document.getElementById("myProfile").style.right = "-680px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";
					
				}
			})
		}

}

function closeNavDetailTr() {
    document.getElementById("openRightBlockTr").style.right = "-847px";
    document.getElementById("overlay").style.display = "none";
    document.body.style.overflow = "auto";
}

function openNavDetailRecord() {
    document.getElementById("moreDetailRecord").style.right = "0";
    document.getElementById("overlay").style.display = "block";
    document.body.style.overflow = "hidden";
    document.getElementById("userDropdown").classList.remove("show");
}

function closeNavDetailRecord() {
    document.getElementById("moreDetailRecord").style.right = "-847px";
    document.getElementById("overlay").style.display = "none";
    document.body.style.overflow = "auto";
}


/*для верхнего окна - Выбор отеля*/

function openNavTop() {
    document.getElementById("topBox").style.top = "0";
    document.getElementById("topBox").style.width = "100%";
    document.getElementById("overlay").style.display = "block";
    document.body.style.overflow = "hidden";
    document.getElementById("userDropdown").classList.remove("show");
}

function closeNavTop() {
    document.getElementById("topBox").style.top = "-600px";
    document.getElementById("overlay").style.display = "none";
    document.body.style.overflow = "auto";
}

/*для правого окна - Редактирование общей информации*/

function openNav4() {
    document.getElementById("editMyMainInfo").style.right = "0";
	
	$(document).mouseup(function (e){ 
		var div = $("#editMyMainInfo"); 
		if (!div.is(e.target) 
			&& div.has(e.target).length === 0) { 			
			document.getElementById("editMyMainInfo").style.right = "-680px";		
			
		}
	});		
}

function closeNav4() {
    document.getElementById("editMyMainInfo").style.right = "-680px";
}

/*для правого окна - Редактирование Паспорт*/

function openNav0() {
    document.getElementById("editMyPassport").style.right = "0";
	
	$(document).mouseup(function (e){ 
		var div = $("#editMyPassport"); 
		if (!div.is(e.target) 
			&& div.has(e.target).length === 0) {
			
			document.getElementById("editMyPassport").style.right = "-680px";		
			
		}
	});
}

function closeNav0() {
    document.getElementById("editMyPassport").style.right = "-680px";
}

/*для правого окна - Редактирование загран. паспорт*/

function openNav1() {
    document.getElementById("editMyMultiPassport").style.right = "0";
	
	$(document).mouseup(function (e){ 
		var div = $("#editMyMultiPassport"); 
		if (!div.is(e.target) 
			&& div.has(e.target).length === 0) { 
			
			document.getElementById("editMyMultiPassport").style.right = "-680px";		
			
		}
	});
}

function closeNav1() {
    document.getElementById("editMyMultiPassport").style.right = "-680px";
}

/*для правого окна - Редактирование рассылки и новости*/

function openNav2() {
    document.getElementById("myNotifications").style.right = "0";
	
	$(document).mouseup(function (e){ 
		var div = $("#myNotifications"); 
		if (!div.is(e.target) 
			&& div.has(e.target).length === 0) { 
			
			document.getElementById("myNotifications").style.right = "-680px";		
			
		}
	});	
}

function closeNav2() {
    document.getElementById("myNotifications").style.right = "-680px";
}

/*для правого окна - Редактирование пароля*/

function openNav3() {
    document.getElementById("myPassword").style.right = "0";
	
	$(document).mouseup(function (e){ 
		var div = $("#myPassword"); 
		if (!div.is(e.target) 
			&& div.has(e.target).length === 0) { 
			
			document.getElementById("myPassword").style.right = "-680px";		
			
		}
	});
}

function closeNav3() {
    document.getElementById("myPassword").style.right = "-680px";
}

/*для правого окна - показать подробности трансфера*/

function openNavDetail(addClass) {
    if (addClass) {
        $('#moreDetail').addClass(addClass);
    }
    document.getElementById("moreDetail").style.right = "0";
    document.getElementById("overlay").style.display = "block";
    document.body.style.overflow = "hidden";
    document.getElementById("userDropdown").classList.remove("show");
	
}

function closeNavDetail() {
    document.getElementById("moreDetail").style.right = "-" + $('#moreDetail').width() + "px";
    document.getElementById("overlay").style.display = "none";
    document.body.style.overflow = "auto";
}


/*для правого окна - создать нового пользователя*/

// function openNavUser() {
//     document.getElementById("createUser").style.right = "0";
//     document.getElementById("overlay").style.display = "block";
//     document.body.style.overflow = "hidden";
//     document.getElementById("userDropdown").classList.remove("show");
// }

// function closeNavUser() {
//     document.getElementById("createUser").style.right = "-680px";
//     document.getElementById("overlay").style.display = "none";
//     document.body.style.overflow = "auto";
// }

/*для левого окна - показать фильтры с 768px*/

function openNavFilters(option_type) {

    var search_results_filter = $('#' + option_type + '_search_results_filter');
    var search_results_filter_mobile = $('#' + option_type + '_search_results_filter_mobile');

    if (search_results_filter.html().length < 100) {
        return false;
    }
    if (search_results_filter_mobile.html().length < 10) {
        search_results_filter_mobile.html(search_results_filter.html());
    }
    search_results_filter_mobile.find('.widget_head').css('margin', '0px');
    search_results_filter_mobile.find('.widget_map').hide();

    document.getElementById("openFilters").style.left = "0";
    document.getElementById("overlay").style.display = "block";
    document.body.style.overflow = "hidden";
    document.getElementById("userDropdown").classList.remove("show");
}

function closeNavFilters() {
    document.getElementById("openFilters").style.left = "-680px";
    document.getElementById("overlay").style.display = "none";
    document.body.style.overflow = "auto";
    $('#hotel_search_results_filter_mobile').find('.widget_map').show();
}

/*для правого окна - Бронирование отеля*/

function openNavBoxBooking() {
    document.getElementById("openBoxBooking").style.right = "0";
    document.getElementById("overlay").style.display = "block";
    document.body.style.overflow = "hidden";
    document.getElementById("userDropdown").classList.remove("show");
}

function closeNavBoxBooking() {
    document.getElementById("openBoxBooking").style.right = "-680px";
    document.getElementById("overlay").style.display = "none";
    document.body.style.overflow = "auto";
}

/*для правого окна -  Аэроэкспресс*/

function openNavBoxBooking() {
    document.getElementById("openBoxAExpress").style.right = "0";
    document.getElementById("overlay").style.display = "block";
    document.body.style.overflow = "hidden";
    document.getElementById("userDropdown").classList.remove("show");
}

function closeBoxAExpress() {
    document.getElementById("openBoxAExpress").style.right = "-847px";
    document.getElementById("overlay").style.display = "none";
    document.body.style.overflow = "auto";
}

/* Для вкладки с пользователями - их фильтр */
if (document.getElementById("filters") != null) {
    var $listUser = $('.users-list').isotope({
        itemSelector: '.user-item',
        layoutMode: 'fitRows',
    });

    $('#filters').on('click', 'button', function () {
        var filterValue = $(this).attr('data-filter');
        $listUser.isotope({filter: filterValue});
    });

    $('.button-group').each(function (i, buttonGroup) {
        var $buttonGroup = $(buttonGroup);
        $buttonGroup.on('click', 'button', function () {
            $buttonGroup.find('.is-checked').removeClass('is-checked');
            $(this).addClass('is-checked');
        });
    });
}

/* Для вкладки компания */
function openTabC(pageName, elmnt, color, border) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent-company");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink-company");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
        tablinks[i].style.borderBottom = "";
    }

    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
    elmnt.style.borderBottom = border;
}

if (document.getElementById("defaultOpenC") != null) {
    // document.getElementById("defaultOpenC").click();
}
/*автозаполнение*/
/*
if (document.querySelector("bs-autocomplete") != null) {
    $.widget("ui.autocomplete", $.ui.autocomplete, {

        _renderMenu: function (ul, items) {
            var that = this;
            ul.attr("class", "nav nav-pills nav-stacked  bs-autocomplete-menu");
            $.each(items, function (index, item) {
                that._renderItemData(ul, item);
            });
        },

        _resizeMenu: function () {
            var ul = this.menu.element;
            ul.outerWidth(Math.min(
                ul.width("").outerWidth() + 1,
                this.element.outerWidth()
            ));
        }
    });
}

(function () {
    "use strict";
    var names = [{
        "id": 1,
        "userName": "Ринат Галимянов"
    }, {
        "id": 2,
        "userName": "Альберт Реутер"
    }];

    $('.bs-autocomplete').each(function () {
        var _this = $(this),
            _data = _this.data(),
            _hidden_field = $('#' + _data.hidden_field_id);

        _this.after('<div class="bs-autocomplete-feedback form-control-feedback"><div class="loader">Loading...</div></div>')
            .parent('.form-group').addClass('has-feedback');

        var feedback_icon = _this.next('.bs-autocomplete-feedback');
        feedback_icon.hide();

        _this.autocomplete({
            minLength: 2,
            autoFocus: true,

            source: function (request, response) {
                var _regexp = new RegExp(request.term, 'i');
                var data = names.filter(function (item) {
                    return item.userName.match(_regexp);
                });
                response(data);
            },

            search: function () {
                feedback_icon.show();
                _hidden_field.val('');
            },

            response: function () {
                feedback_icon.hide();
            },

            focus: function (event, ui) {
                _this.val(ui.item[_data.item_label]);
                event.preventDefault();
            },

            select: function (event, ui) {
                _this.val(ui.item[_data.item_label]);
                _hidden_field.val(ui.item[_data.item_id]);
                event.preventDefault();
            }
        })
            .data('ui-autocomplete')._renderItem = function (ul, item) {
            return $('<li></li>')
                .data("item.autocomplete", item)
                .append('<a>' + item[_data.item_label] + '</a>')
                .appendTo(ul);
        };
    });
})();
*/
$(document).ready(function () {
    $(".datepicker").datepicker({
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
            'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
            'Октябрь', 'Ноябрь', 'Декабрь'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        firstDay: 1,
    });
});

/*добавление формы откуда-куда вкладка Авиа*/
/*
$(document).ready(function () {
    var maxGroup = 10;
    //add more fields group
    $(".addMore").click(function () {
        if ($('body').find('.fieldGroup').length <= maxGroup) {
            var fieldHTML = '<div class="form-group fieldGroup">' + $(".fieldGroupCopy").html() + '</div>';
            $('body').find('.fieldGroup:last').before(fieldHTML);
        } else {
            alert('Maximum ' + maxGroup + ' groups are allowed.');
        }
    });
    //remove fields group
    $("body").on("click", ".remove", function () {
        $(this).parents(".fieldGroup").remove();
    });
});
*/

/*добавление формы откуда-куда вкладка Поезда*/

/*$(document).ready(function () {
    var maxGroup = 10;
    //add more fields group
    $(".addMoreTrain").click(function () {
        if ($('body').find('.fieldGroupTrain').length <= maxGroup) {
            var fieldHTML = '<div class="form-group fieldGroupTrain">' + $(".fieldGroupCopyTrain").html() + '</div>';
            $('body').find('.fieldGroupTrain:last').before(fieldHTML);
        } else {
            alert('Maximum ' + maxGroup + ' groups are allowed.');
        }
    });
    //remove fields group
    $("body").on("click", ".remove", function () {
        $(this).parents(".fieldGroupTrain").remove();
    });
});*/

/*добавление формы откуда-куда вкладка Автобусы*/

/*$(document).ready(function () {
    var maxGroup = 10;
    //add more fields group
    $(".addMoreBus").click(function () {
        if ($('body').find('.fieldGroupBus').length <= maxGroup) {
            var fieldHTML = '<div class="form-group fieldGroupBus">' + $(".fieldGroupCopyBus").html() + '</div>';
            $('body').find('.fieldGroupBus:last').before(fieldHTML);
        } else {
            alert('Maximum ' + maxGroup + ' groups are allowed.');
        }
    });
    //remove fields group
    $("body").on("click", ".remove", function () {
        $(this).parents(".fieldGroupBus").remove();
    });
});*/

/* Для переключения между табами на странице поиска рейсов Туда-Обратно*/

/*
function openOF(pageName, elmnt, color, colorFont) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent-filters");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablink-filters");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].style.backgroundColor = "";
        tablinks[i].style.color = "";
    }
    document.getElementById(pageName).style.display = "block";
    elmnt.style.backgroundColor = color;
    elmnt.style.color = colorFont;
}

if (document.getElementById("defaultOpen_filter") != null) {
    document.getElementById("defaultOpen_filter").click();
}
*/


/*скрипт для поиска пользователей на странице добавления группы*/

$(document).ready(function () {
    $("#userInput").on("keyup", function () {
        var value = $(this).val().toLowerCase();
        $("#userTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });
});


$(document).ready(function () {
    Responsive_menu();
    wowanimation();
})


$(document).ajaxStart(function () {
    $('#ajaxloading').fadeIn(500);  // show loading indicator
});

$(document).ajaxStop(function () {
    $('#ajaxloading').fadeOut(500);  // hide loading indicator
});

function nl2br(str, is_xhtml) {
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function showAlert(error, vReloadAfterClose) {

    $("#show_alert_modal").find('.content-modal-body').html(nl2br(error))
    $("#show_alert_modal").modal()

    if (vReloadAfterClose) {
        $("#show_alert_modal").on('hidden.bs.modal', function () {
            window.location.reload(true);
            return false;
        });
    }

}

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

//клик по кнопке "да" (подтверждение действия) в диалоге подтверждения действий с трипом
$(document).on('click', '#btn_answer_yes', function () {
    var btn = $(this);
    var url = btn.attr('href');
    var ftr = btn.attr('data-ftr');
    var tid = btn.attr('data-tid');
    if (ftr) {
        window[ftr](tid);
        return false;
    }
    if (url) {
        window.location = url
    }
    return false;
})

function IsJsonString(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}

$(document).ready(function () {
    //клик по кнопке "Условия применения тарифа" (авиа)
    $(document).on('click', '.avia_show_upt', function () {
        $(this).next('.avia_upt_info').slideToggle('fast');
    })
})
setTimeout(function(){

// дополнительные условия для закрытия шторки
divMoreD = document.getElementById('moreDetailRecord');
divCreate = document.getElementById('createUser');

divOpT = document.getElementById('openRightBlockTr');
divTicket = document.getElementById('moreDetail');

	if(divMoreD){ // посмотреть детали
		widthMoreD = window.getComputedStyle(divMoreD).right;
		
		if (widthMoreD == "-847px"){
			$(document).mouseup(function (e){ 
				var profil = $("#myProfile"); 
				if (!profil.is(e.target) 
					&& profil.has(e.target).length === 0 
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px" 
					&& $('#myPassword').css('right') == "-680px"
					&& $('#moreDetailRecord').css('right') == "-847px"
					&& $('#editMyMultiPassport').css('right') == "-680px"
					&& $('#myNotifications').css('right') == "-680px"){ 
					document.getElementById("myProfile").style.right = "-680px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";		
					
				}
			});
			
			$(document).mouseup(function (e){ 
				var moreD = $("#moreDetailRecord"); 
				if (!moreD.is(e.target) 
					&& moreD.has(e.target).length === 0 
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px" 
					&& $('#myPassword').css('right') == "-680px"
					&& $('#myProfile').css('right') == "-680px"
					&& $('#editMyMultiPassport').css('right') == "-680px"
					&& $('#myNotifications').css('right') == "-680px"){ 
					document.getElementById("moreDetailRecord").style.right = "-847px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";		
					
				}
			});
		}
		if(widthMoreD == "0"){
			$(document).mouseup(function (e){ 
				var profil = $("#myProfile"); 
				if (!profil.is(e.target) 
					&& profil.has(e.target).length === 0 
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px" 
					&& $('#myPassword').css('right') == "-680px"
					&& $('#moreDetailRecord').css('right') == "-847px"
					&& $('#myNotifications').css('right') == "-680px"){ 
					document.getElementById("myProfile").style.right = "-680px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";
				}
			});
		}
	}
	else if( $(".table_users").length) { // сотрудники alert("staff_show_edit");
		
		$(".staff_show_edit").click(function() {			
			$(document).mouseup(function (e){ 
				var div = $("#createUser"); 
				if (!div.is(e.target) 
					&& div.has(e.target).length === 0 
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px" 
					&& $('#myPassword').css('right') == "-680px"
					&& $('#editMyMultiPassport').css('right') == "-680px"
					&& $('#myProfile').css('right') == "-680px"
					&& $('#myNotifications').css('right') == "-680px") { 			
					document.getElementById("createUser").style.right = "-680px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";					
				}
			});			
			$(document).mouseup(function (e){ 
				var div = $("#myProfile"); 
				if (!div.is(e.target) 
					&& div.has(e.target).length === 0 
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px" 
					&& $('#myPassword').css('right') == "-680px"
					&& $('#editMyMultiPassport').css('right') == "-680px"
					&& $('#createUser').css('right') == "-680px"		
					&& $('#myNotifications').css('right') == "-680px"){ 
					document.getElementById("myProfile").style.right = "-680px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";	
					
				}
			})
			
		});
	} 
 	/*  else if ($('#openRightBlockTr').length) {
			alert("openRightBlockTr");
			$(document).mouseup(function (e){ 
				var divs = $("#openRightBlockTr");
				if (!divs.is(e.target)
					&& divs.has(e.target).length === 0
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px" 
					&& $('#myPassword').css('right') == "-680px"
					&& $('#editMyMultiPassport').css('right') == "-680px"
					&& $('#myProfile').css('right') == "-680px"
					&& $('#myNotifications').css('right') == "-680px"){ 
					document.getElementById("openRightBlockTr").style.right = "-680px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";
				}
			});

			$(document).mouseup(function (e){ 
				var divs = $("#myProfile");
				if (!divs.is(e.target)
					&& divs.has(e.target).length === 0
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px" 
					&& $('#myPassword').css('right') == "-680px"
					&& $('#editMyMultiPassport').css('right') == "-680px"
					&& $('#openRightBlockTr').css('right') == "-680px"
					&& $('#myNotifications').css('right') == "-680px"){ 
					document.getElementById("myProfile").style.right = "-680px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";
					
				}
			})
	}  */
	/*  else if ($('#moreDetail').css('right') == "-847px") {

			$(document).mouseup(function (e){
				var divs = $("#moreDetail");
				if (!divs.is(e.target)
					&& divs.has(e.target).length === 0
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px"
					&& $('#myPassword').css('right') == "-680px"
					&& $('#editMyMultiPassport').css('right') == "-680px"
					&& $('#myProfile').css('right') == "-680px"
					&& $('#myNotifications').css('right') == "-680px"){
					document.getElementById("moreDetail").style.right = "-680px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";
				}
			});

			$(document).mouseup(function (e){
				var divs = $("#myProfile");
				if (!divs.is(e.target)
					&& divs.has(e.target).length === 0
					&& $('#editMyMainInfo').css('right') == "-680px"
					&& $('#editMyPassport').css('right') == "-680px"
					&& $('#myPassword').css('right') == "-680px"
					&& $('#editMyMultiPassport').css('right') == "-680px"
					&& $('#moreDetail').css('right') == "-680px"
					&& $('#myNotifications').css('right') == "-680px"){
					document.getElementById("myProfile").style.right = "-680px";
					document.getElementById("overlay").style.display = "none";
					document.body.style.overflow = "auto";

				}
			})
	}  */
	 else if ($('#moreDetail').length){
				$(document).mouseup(function (e){
					var divs = $("#moreDetail");
					if (!divs.is(e.target)
						&& divs.has(e.target).length === 0
						&& $('#editMyMainInfo').css('right') == "-680px"
						&& $('#editMyPassport').css('right') == "-680px"
						&& $('#myPassword').css('right') == "-680px"
						&& $('#editMyMultiPassport').css('right') == "-680px"
						&& $('#myProfile').css('right') == "-680px"
						&& $('#myNotifications').css('right') == "-680px") {
						    document.getElementById("moreDetail").style.right = "-" + $('#moreDetail').width() + "px";
							document.getElementById("overlay").style.display = "none";
							document.body.style.overflow = "auto";
					}
				});

				$(document).mouseup(function (e){
					var divs = $("#myProfile");
					if (!divs.is(e.target)
						&& divs.has(e.target).length === 0
						&& $('#editMyMainInfo').css('right') == "-680px"
						&& $('#editMyPassport').css('right') == "-680px"
						&& $('#myPassword').css('right') == "-680px"
						&& $('#editMyMultiPassport').css('right') == "-680px"
						&& $('#moreDetail').css('right') == "-847px"
						&& $('#myNotifications').css('right') == "-680px"){
						document.getElementById("myProfile").style.right = "-680px";
						document.getElementById("overlay").style.display = "none";
						document.body.style.overflow = "auto";

					}
				})

		}

	else { // в остальных случаях	alert("not");
		
		$(document).mouseup(function (e) {		
			var profil = $("#myProfile"); 
			if (!profil.is(e.target) 
				&& profil.has(e.target).length === 0 
				&& $('#editMyMainInfo').css('right') == "-680px"
				&& $('#editMyPassport').css('right') == "-680px" 
				&& $('#myPassword').css('right') == "-680px"
				&& $('#editMyMultiPassport').css('right') == "-680px"
				&& $('#myNotifications').css('right') == "-680px"){ 
				document.getElementById("myProfile").style.right = "-680px";
				document.getElementById("overlay").style.display = "none";
				document.body.style.overflow = "auto";
			}			
		});				
	}
}, 3000);
//для фиксирования блока с ценой с дополнтельные услуги
if ( $(".widget_add_box").length) {
	var div_top = $('.widget_add_box').offset().top;

	$(window).scroll(function() {
		var window_top = $(window).scrollTop() - 0;
		if (window_top > div_top) {
			if (!$('.widget_add_box').is('.sticky-box')) {
				$('.widget_add_box').addClass('sticky-box');
			}
		} else {
			$('.widget_add_box').removeClass('sticky-box');
		}
	});	
}	
