
// Tabs
(function() {

    $('.tour__nav a').on('click touchstart', function(e){
        e.preventDefault();

        var tab = $($(this).attr("data-target"));
        var box = $(this).closest('.tour');

        $(this).closest('.tour__nav').find('a').removeClass('active');
        $(this).addClass('active');

        box.find('.tour__tab').removeClass('active');
        box.find(tab).addClass('active');
    });

}());


// route
(function() {

    $('.tour__routes_toggle').on('click touchstart', function(e){
        e.preventDefault();

        $(this).closest('.tour__tab').find('.tour__routes_hide').toggleClass('open');
        $(this).toggleClass('open');
    });

}());


// Update 26.07.2019

// SVG IE11 support
svg4everybody();
