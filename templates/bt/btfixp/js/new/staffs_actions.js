/**
* Скрипт модального окна для действий над сотрудниками
* Добавление/редактирование
*
* Класс вызова модального окна для элемента 
* @class staff_show_edit
*
* Дополнительные аттрибуты вызова (* обязателен)
*
* Для добавления:
* @data action*, close("close", "reload", "none"), form-title, submit-title;
* Для редактирования:
* @data action*, staff-id*, close("close", "reload", "none"), form-title, submit-title;
**/

$(function(){
    $(document).on('click', '.staff_show_edit', function(){
        var action      = $(this).data("action");
        var close       = ($(this).data("close") == '' || $(this).data("close") == undefined) ? 'close' : $(this).data("close");
        var staffId     = ($(this).data("staff-id") == '' || $(this).data("staff-id") == undefined) ? null : $(this).data("staff-id");
        var modalTitle  = ($(this).data("form-title") == '' || $(this).data("form-title") == undefined) ? 'Форма сотрудника' : $(this).data("form-title");
        var submitTitle = ($(this).data("submit-title") == '' || $(this).data("submit-title") == undefined) ? 'Сохранить' : $(this).data("submit-title");

        switch (action) {
            case 'add':
                $.ajax({
                    type: 'POST',
                    url: '/index.php?do=getinfoforaddstaff',
                    data: {},
                    async: false,
                    success: function(result){
                        result = JSON.parse(result);
        
                        if (result.type == 'success') {
                            $('body').append(result.content);
                            
                            $('#staff_modal_select_citizenship').children('select').styler();
                            $('#staff_modal_select_gender').children('select').styler();
                            $('#staff_modal_select_unit').children('select').styler();

                            $(".staffActionModalTitle").html(modalTitle);
                            $(".staffActionModalSubmit").html(submitTitle);
                            $(".staffActionModalSubmit").attr('data-action', action);
                            $(".staffActionModalSubmit").attr('data-close', close);

                            openNavUser();
                        } else if (result.type == 'error') {
                            alert(result.message);
                        }
                    },
                });
                
                break;
            case 'edit':
                    $.ajax({
                        type: 'POST',
                        url: '/index.php?do=getinfoforeditstaff',
                        data: {
                            staffId: staffId
                        },
                        async: false,
                        success: function(result){
                            result = JSON.parse(result);

                            if (result.type == 'success') {
                                $('body').append(result.content);

                                $('#staff_modal_select_citizenship').children('select').styler();
                                $('#staff_modal_select_gender').children('select').styler();
                                $('#staff_modal_select_unit').children('select').styler();

                                $(".staffActionModalTitle").html(modalTitle);
                                $(".staffActionModalSubmit").html(submitTitle);
                                $(".staffActionModalSubmit").attr('data-action', action);
                                $(".staffActionModalSubmit").attr('data-close', close);

                                openNavUser();
                            } else if (result.type == 'error') {
                                alert(result.message);
                            }
                        },
                    });

                break;
        }
    });

    $(document).on('click', '.staffActionModalSubmit', function(){
        var action = $(this).data('action');
        var close  = $(this).data('close');

        var formData = new FormData($('#staffActionModal').get(0));

        switch (action) {
            case 'add':
                $.ajax({
                    type: 'POST',
                    url: '/index.php?do=addstaff',
                    data: formData,
                    processData: false,
                    contentType: false,
                    async: false,
                    success: function(result){
                        result = JSON.parse(result);

                        if (result.type == 'success') {
                            switch (close) {
                                case 'close':
                                    closeNavUser();
                                    break;
                                case 'reload':
                                    location.reload();
                                    break;
                                default :
                                    break;
                            }
                        } else if (result.type == 'error') {
                            alert(result.message);
                        }
                    },
                });

                break;
            case 'edit':
                $.ajax({
                    type: 'POST',
                    url: '/index.php?do=editstaff',
                    data: formData,
                    processData: false,
                    contentType: false,
                    async: false,
                    success: function(result){
                        result = JSON.parse(result);

                        if (result.type == 'success') {
                            switch (close) {
                                case 'close':
                                    closeNavUser();
                                    break;
                                case 'reload':
                                    location.reload();
                                    break;
                                default :
                                    break;
                            }
                        } else if (result.type == 'error') {
                            alert(result.message);
                        }
                    },
                });

                break;
        }
    });

    $(document).on('click', '#staffActionModalCloseButton', function(){
        closeNavUser();
    });

    $(document).on('keyup', '.staffActionModalSurname', function(){ 
        if ($(this).val().length > 0) {
            var text = $(this).val();

            $('.staffActionModalLSurname').val(convertToTranslit(text));
        }
    });
    
    $(document).on('keyup', '.staffActionModalFirstName', function(){ 
        if ($(this).val().length > 0) {
            var text = $(this).val();

            $('.staffActionModalLName').val(convertToTranslit(text));
        }
    });

    $(document).on('click', '.staffActionModalCheckboxUserLabel', function(){
        if ($('.staffActionModalCheckboxUser').is(':checked')) {
            $('.staffActionModalCheckboxUser').attr('checked', 'checked');
        } else {
            $('.staffActionModalCheckboxUser').removeAttr('checked');
        }
    });

    function openNavUser() {
        document.getElementById("createUser").style.right = "0";
        document.getElementById("overlay").style.display = "block";
        document.body.style.overflow = "hidden";
        document.getElementById("userDropdown").classList.remove("show");
    }
    
    function closeNavUser() {
        document.getElementById("createUser").style.right = "-680px";
        document.getElementById("overlay").style.display = "none";
        document.body.style.overflow = "auto";

        $('#createUser').remove();
    }

    function convertToTranslit(text){
        $.ajax({
            type: 'POST',
            url: '/index.php?do=converttotranslit',
            data: {
                text: text.toLowerCase()
            },
            async: false,
            success: function(result){
                result = JSON.parse(result);

                if (result.type == 'success') {
                    text = result.content.replace(/\b\w/g, function(b){ 
                        return b.toUpperCase() 
                    });
                }
            },
        });

        return text;
    }
})