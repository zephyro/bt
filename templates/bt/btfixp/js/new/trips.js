$('input#trip_date_start').datepicker({
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
        'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
        'Октябрь', 'Ноябрь', 'Декабрь'],
    dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    firstDay: 1,
    autoClose: true,
    // minDate: 1,
    // dateFormat: 'dd MM',
    dateFormat: 'dd.mm.yy',
    onSelect: function (date_txt, obj) {
        $('#trips_dates_and_text').trigger('submit');
        /*
                var date2 = $('input#trip_date_start').datepicker('getDate', '+7d');
                date2.setDate(date2.getDate() + 7);
                setTimeout(function () {
                    $('input#trip_date_end').datepicker("option", 'minDate', date2)
                    $('input#trip_date_end').datepicker("setDate", date2).datepicker("show")
                }, 100)
        */
    }
});
$('input#trip_date_end').datepicker({
    monthNames: ['Январь', 'Февраль', 'Март', 'Апрель',
        'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь',
        'Октябрь', 'Ноябрь', 'Декабрь'],
    dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
    firstDay: 1,
    autoClose: true,
    // minDate: 1,
    // dateFormat: 'dd MM',
    dateFormat: 'dd.mm.yy',
    onSelect: function (date_txt, obj) {
        $('#trips_dates_and_text').trigger('submit');
    }
});


$(document).ready(function () {

    //клик по кнопкам сортировки
    $(document).on('click', '.trips_order_by', function () {
        var order_by = $(this).attr('data-order_by');
        window.location = '/index.php?do=trips&order_by=' + order_by;
        return false;
    })
    //клик по кнопкам "Ждут согласования"/"Отклоненные", "черновики"
    $(document).on('click', 'input[name=trip_filter_status]', function () {
        var status = $(this).attr('data-value');
        window.location = '/index.php?do=trips&trips_status=' + status;
        return false;
    })

    //клик по кнопке "сбросить" (сброс всех параметров сортировки-поика-фильтра)
    $(document).on('click', '#trips_reset_filter_btn', function () {
        window.location = '/index.php?do=trips&trips_reset_filter=1';
        return false;
    })


    //клик по кнопке "показать детали" записи трипа
    $(document).on('click', '.show_record_details', function () {

        var btn = $(this);
        var trip_id = btn.attr('data-trip_id');
        var record_id = btn.attr('data-record_id');

        $.ajax({
            type: "POST",
            url: '/index.php?do=trips_ajax',
            data: {
                'action': 'show_record_details',
                'trip_id': trip_id,
                'record_id': record_id,
            },
            success: function (res) {

                if (!IsJsonString(res)) {
                    showAlert('Возникла непредвиденная ошибка (' + res + '). Пожалуйста, попробуйте позже или обратитесь в службу поддержки.');
                    return false;
                }

                var result = jQuery.parseJSON(res);

                $('#headerTitleDataRecord').html(result.headerTitle);
                $('#moreDetail').html(result.content);
                openNavDetailRecord();

                return false;

            },
            error: function () {
                showAlert('Ошибка получения данных. Пожалуйста, повторите еще раз или обратитесь в службу поддержки.');
            },
            complete: function () {
            }
        });

        return false;
    })

})

