<div id="createUser" class="sidenav"> 
	<a href="javascript:void(0)" class="closebtn" id="staffActionModalCloseButton">&times;</a>
	<div class="page_block" id="settingDetail">
		<div class="page_block_header">
			<h2 class="staffActionModalTitle"></h2>
		</div>
		<div class="page_block_info_more">
			<div class="descr_block_info_more">
				<p><span>Общая информация</span></p>
				<p>
					Заполняйте данные "как в паспорте". Это важно для правильного бронирования услуг, билетов и отелей.
                </p>
			</div>
		</div>
		<form method="post" id="staffActionModal" action="">  
			<input type="hidden" class="staffActionModalStaffId" name="id" value="{staff_id}">
			<div class="page_block_info_more">			
				<div class="form_group">												
					<input class="form_control pl-shift-right staffActionModalSurname" type="text" name="surname" id="" placeholder="Фамилия" value="{surname}">
				</div>
				<div class="form_group">							
					<input class="form_control pl-shift-right staffActionModalFirstName" type="text" name="first_name" id="" placeholder="Имя" value="{first_name}">							
				</div>
				<div class="form_group">							
					<input class="form_control pl-shift-right staffActionModalMiddleName" type="text" name="middle_name" id="" placeholder="Отчество" value="{middle_name}">							
				</div>		
				<div class="form_group">																										
					<div class="selct_box" id="staff_modal_select_citizenship">
                        {select_country}
					</div>					
				</div><br>
				<div class="form_group">							
					<input class="form_control pl-shift-right staffActionModalBDate" type="text" name="bdate" id="" placeholder="Дата рождения" value="{bdate}">							
				</div>
				<div class="form_group">							
					<div class="selct_box" id="staff_modal_select_gender">
                        {select_gender}
					</div>	
				</div>
			</div>
			<div class="page_block_info_more">
				<div class="form_group">							
					<input class="form_control pl-shift-right staffActionModalStaffEmail" type="text" name="staff_email" id="" placeholder="Email" value="{staff_email}">							
				</div>
				<div class="form_group">												
					<input class="form_control pl-shift-right staffActionModalStaffPhome" type="text" name="staff_phone" id="" placeholder="Мобильный телефон" value="{staff_phone}">
				</div>
				<div class="control-group staffActionModalCheckboxUserLabel">
                    {user_checkbox}												
				</div><br>
				<div class="form_group">												
					<div class="selct_box" id="staff_modal_select_unit">
                        {select_unit}
					</div>
				</div><br>
				<div class="form_group">							
					<input class="form_control pl-shift-right staffActionModalOffice" type="text" name="office" id="" placeholder="Должность" value="{office}">							
				</div>
			</div>
			<div class="page_block_info_more">
				<div class="form_group">							
					<input class="form_control pl-shift-right staffActionModalPassport" type="text" name="passport" id="" placeholder="Гражданский паспорт (серия и номер без пробелов)" value="{passport}">							
				</div>
				<div class="form_group">							
					<input class="form_control pl-shift-right staffActionModalMultiPass" type="text" name="multipass" id="" placeholder="Загран. паспорт (серия и номер без пробелов)" value="{multipass}">							
				</div>
				<div class="form_group">							
					<input class="form_control pl-shift-right staffActionModalMultidate" type="text" name="multidate" id="" placeholder="Срок действия" value="{multidate}">							
				</div>
				<div class="form_group">							
					<input class="form_control pl-shift-right staffActionModalLSurname" type="text" name="lsurname" id="" placeholder="Фамилия латиницей" value="{lsurname}">							
				</div>
				<div class="form_group">							
					<input class="form_control pl-shift-right staffActionModalLName" type="text" name="lname" id="" placeholder="Имя латиницей" value="{lname}">							
				</div>
	
				<div class="form_group save_btn">
					<button type="button" data-action="" class="btn_sidenav staffActionModalSubmit"></button>
				</div>
			</div>
		</form>
	</div>
</div>